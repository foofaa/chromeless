# Chromeless for Android
## What?/Why?
I build small HTML5 web apps for use on my PC and devices. I wanted a simple Android container for these apps to run in - not a full browser, but more functional than the HTML Viewer app. This repository includes a wrap-up of all the tips & tricks I picked up whilst building this simple container. Note: this is not a complete Android/HTML5app project - you'll need to create your own project and drop these files into it.

## Code Notes
### AndroidManifest.xml
- 'meta-data' element: yup, you need to opt out of Google tracking your WebView usage, FFS.
- 'activity' attributes: mostly to suppress reloading of the HTML5app page upon returning from elsewhere.
- 'intent-filter' elements: matches both the 'file' and 'content' schemes: the former occurs when opening an html file from a file manager (i.e. Android app receives a URI like 'file:///storage/emulated/0/...'; the latter occurs when opening a launcher shortcut (i.e. Android app receives a URI like 'content://com.android.htmlfileprovider/sdcard/...').

### ActivityMain.java
- The Activity is set up to launch external links (i.e. 'http[s]://') in a 'proper' browser of your choice. Internal links (i.e. 'file://' or relative URIs) are opened inside Chromeless.
- Incoming 'content' URIs are rewritten to be 'file' URIs (via some rather hack-ey code, sorry).
- WebSettings: permits JavaScript to run, local file system resource access, and page zooming. Also, 'setKeepScreenOn()' is set to 'true' to stop the screen from timing out when the HTML5app is in use.
- WebAppInterface: an example of an Android/HTML5app bridge. The one here facilitates 'copy to clipboard' from within the HTML5app.
- The rest of the code configures 'Back' button functionality, and helps to suppress reloading of the HTML5app page upon returning from elsewhere.
- The default file ('file:///android_asset/index.html') is displayed if you open this app, rather than use it to open an HTML5app. This file is not included in this repository.

### HTML5app.html
- Uses the [jQuery Mobile Web Framework](https://jquerymobile.com/) to do it's magic. You'll need to grab local copies of the referenced resources, or direct them to your favourite CDN.
- I've included my 'copy to clipboard' function, c2c(), (including the 'Android' JavaScript interface, as per 'ActivityMain.java') in this stub document. I use jQuery to bind this function to the click event of 'c2c'-classed elements (e.g. spans) within my HTML body. Most variants of this code inject the fallback into the document root, but I found that this can make long documents jump around - my variant uses the clicked element (e.g. the span) as the temporary container, which stops the jumping.
