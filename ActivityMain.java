package com.xayin.android.chromeless;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.ClipboardManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/*
 * ////////////////////////////////////////////////////////////////////////////
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * Title of work: Chromeless
 * Attribute work to name: Chris Molloy
 * Attribute work to URL: https://chrismolloy.com/
 * Format of work: Software
 * ////////////////////////////////////////////////////////////////////////////
*/

public class ActivityMain extends Activity {

	class WebAppInterface {
		Context ccContext;

		WebAppInterface(Context _Context) {
			ccContext = _Context;
		}

		@JavascriptInterface
		public void c2c(String _Word) {
			final ClipboardManager mClipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
			mClipboard.setText(_Word);
			Toast.makeText(ccContext, "Clipboard: " + _Word, Toast.LENGTH_SHORT).show();
		}
	}

	private static final String C_DEFAULT_URL = "file:///android_asset/index.html";

	private WebView cWebView;

	@SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
	@Override
	protected void onCreate(final Bundle _Bundle) {
		super.onCreate(_Bundle);

		Uri mUri = getIntent().getData();
		String sUrl = mUri == null ? C_DEFAULT_URL : mUri.toString();
		sUrl = sUrl.length() == 0 ? C_DEFAULT_URL : sUrl;
		if (sUrl.startsWith("content:")) sUrl = "file://" + Environment.getExternalStorageDirectory().getAbsolutePath() + sUrl.substring(sUrl.indexOf("/sdcard/") + 7);

		cWebView = new WebView(this);
		cWebView.setKeepScreenOn(true);

		WebSettings mWebSettings = cWebView.getSettings();
		mWebSettings.setUseWideViewPort(true);
		mWebSettings.setBuiltInZoomControls(true);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) mWebSettings.setDisplayZoomControls(false);
		mWebSettings.setJavaScriptEnabled(true);
		cWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) mWebSettings.setAllowUniversalAccessFromFileURLs(true);

		cWebView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView _WebView, String _Url) {
				Uri mmUri = Uri.parse(_Url);
				String sScheme = mmUri.getScheme(); // returns null if this is a relative URI
				if (sScheme != null && (sScheme.equals("http") || sScheme.equals("https"))) {
					try {
						startActivity(new Intent(Intent.ACTION_VIEW, mmUri));
						return true; // open external link in 'proper' browser
					}
					catch (ActivityNotFoundException e) {}
				}
				return false; // open internal link in this WebView
			}
		});

		setContentView(cWebView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		if (_Bundle == null) {
			cWebView.loadUrl(sUrl);
		}
	}

	@Override
	public boolean onKeyDown(int _KeyCode, KeyEvent _KeyEvent) {
		if ((_KeyCode == KeyEvent.KEYCODE_BACK) && cWebView.canGoBack()) {
			cWebView.goBack();
			return true;
		}

		return super.onKeyDown(_KeyCode, _KeyEvent);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		cWebView.saveState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		cWebView.restoreState(savedInstanceState);
	}

	@Override
	protected void onDestroy() {
		// BUG HACK - https://stackoverflow.com/questions/5267639/how-to-safely-turn-webview-zooming-on-and-off-as-needed
		cWebView.getSettings().setBuiltInZoomControls(true);
		cWebView.setVisibility(View.GONE);

		cWebView.destroy();
		cWebView = null;

		super.onDestroy();
	}

}
